﻿using UnityEngine;
using System.Collections;

public class UIScreen : MonoBehaviour {

    public virtual void OnScreenLoaded()
    {

    }

    public virtual void OnBackButtonClicked()
    {

    }

    public void DeactivateScreen()
    {
        transform.gameObject.SetActive(false);
    }

    public virtual void RefreshScreen()
    {

    }
}
