﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KnittingScreenController : UIScreen {

    private const string HEADER_NO_PROJECT_TOKEN = "HEADER_NO_PROJECT";
    private const string PROJECT_COMPLETE_TOKEN = "MESSAGE_PROJECT_COMPLETE";
    private const string PROJECT_START_TOKEN = "MESSAGE_PROJECT_START";

    private const string BUTTON_STORE = "BUTTON_STORE";
    private const string BUTTON_PROJECTS = "BUTTON_PROJECTS";
    private const string BUTTON_YARN = "BUTTON_YARN";

    GameController root;
    PlayerDataManager m_PlayerData;

    //UI
    Text m_tCurrentProjectProgress;
    Text m_tCurrentProjectName;
    GameObject m_LeftPanel;

    // Pattern Data
    Pattern m_CurrentPattern;

    // Button Texts
    Text m_tStoreButtonText;
    Text m_tProjectsButtonText;
    Text m_tYarnButtonText;

    // Needles
    private GameObject[] m_Needles = new GameObject[2];
    private Vector3 m_InitLeftAngle, m_InitRightAngle;

    // Use this for initialization
    void Awake () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        m_PlayerData = root.GetPlayerData();
        GameObject projectButton = transform.FindChild("ProjectButton").gameObject;
        m_tCurrentProjectProgress = transform.FindChild("ProjectButton").FindChild("Text").GetComponent<Text>();
        m_tCurrentProjectName = transform.FindChild("Text_ProjectName").GetComponent<Text>();
        m_tStoreButtonText = transform.FindChild("StoreButton").FindChild("Text").GetComponent<Text>();
        m_tProjectsButtonText = transform.FindChild("ProjectsButton").FindChild("Text").GetComponent<Text>();
        m_tYarnButtonText = transform.FindChild("YarnStashButton").FindChild("Text").GetComponent<Text>();
        m_Needles[0] = projectButton.transform.FindChild("Left_Needle").gameObject;
        m_Needles[1] = projectButton.transform.FindChild("Right_Needle").gameObject;
        m_InitLeftAngle = m_Needles[0].transform.localEulerAngles;
        m_InitRightAngle = m_Needles[1].transform.localEulerAngles;

        // First time setup
        m_CurrentPattern = new Pattern("Hat", 10, 30, 10, 1); //Dummy value for testing
        m_PlayerData.StartProject();
        m_tCurrentProjectProgress.text = "Rows: " + m_PlayerData.GetCurrentProjectRows() + "/" + m_CurrentPattern.GetPatternLength();
        if (m_PlayerData.GetIsActiveProject())
        {
            m_tCurrentProjectName.text = m_CurrentPattern.GetName();
        }
        else
        {
            m_tCurrentProjectName.text = Localizer.Loc(HEADER_NO_PROJECT_TOKEN);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}

    public override void OnScreenLoaded()
    {
        base.OnScreenLoaded();
        //Init button text
        m_tStoreButtonText.text = Localizer.Loc(BUTTON_STORE);
        m_tProjectsButtonText.text = Localizer.Loc(BUTTON_PROJECTS);
        m_tYarnButtonText.text = Localizer.Loc(BUTTON_YARN);

        //Check if there is a project loaded
        if (!m_PlayerData.GetIsActiveProject())
        {
            //If not, try to grab next pattern off of queue
            TryGetNextProject();
        }

        if (m_PlayerData.GetIsActiveProject())
        {
            m_tCurrentProjectName.text = m_CurrentPattern.GetName();
        }
        else
        {
            m_tCurrentProjectName.text = Localizer.Loc(HEADER_NO_PROJECT_TOKEN);
        }
    }

    public void OnStoreButtonClicked()
    {
        root.TransitionUIScreen(GameController.UIScreenEnum.Store);
    }

    public void OnProjectsButtonClicked()
    {
        root.TransitionUIScreen(GameController.UIScreenEnum.Projects);
    }

    public void OnYarnButtonClicked()
    {
        root.TransitionUIScreen(GameController.UIScreenEnum.Yarn);
    }

    public void OnCurrentProjectTapped()
    {
        if (!m_PlayerData.GetIsActiveProject())
        {
            TryGetNextProject();
        }
        else
        {
            StartCoroutine(AnimateNeedles());
            m_PlayerData.AddProjectRows(1);  //TODO: 1 for now, might have upgrade later to do multiple rows per tap
            m_tCurrentProjectProgress.text = "Rows: " + m_PlayerData.GetCurrentProjectRows() + "/" + m_CurrentPattern.GetPatternLength();
            CheckAndHandleProjectCompleted();
            root.UpdateStatsScreen();
        }
    }

    public void CheckAndHandleProjectCompleted()
    {
        if (m_PlayerData.GetCurrentProjectRows() >= m_CurrentPattern.GetPatternLength())
        {
            Debug.Log("Project complete: " + m_CurrentPattern.GetName());
            // Add skill, for now sell immediately
            m_PlayerData.AddSkill(m_CurrentPattern.GetSkillPointsValue());
            m_PlayerData.AddMoney(m_CurrentPattern.GetBaseSellValue());
            m_PlayerData.ClearProjectProgress();
            root.UpdateStatsScreen();

            m_tCurrentProjectProgress.text = Localizer.Loc(PROJECT_COMPLETE_TOKEN);
        }
    }

    private void TryGetNextProject()
    {
        if (root.GetProjectQueue().IsQueueEmpty())
        {
            root.ActivatePopUpMessage(PopUpController.PopUpContext.NeedMoreProjectsInQueue, null);
        }
        else
        {
            m_CurrentPattern = root.GetNextPatternFromQueue();
            m_PlayerData.StartProject();
            if (m_PlayerData.GetIsActiveProject())
            {
                m_tCurrentProjectName.text = m_CurrentPattern.GetName();
            }
            else
            {
                m_tCurrentProjectName.text = Localizer.Loc(HEADER_NO_PROJECT_TOKEN);
            }
            m_tCurrentProjectProgress.text = Localizer.Loc(PROJECT_START_TOKEN);
        }
    }

    private IEnumerator AnimateNeedles()
    {
        int rotationSpeed = 500;
        float leftAngle = m_InitLeftAngle.z;
        float rightAngle = m_InitRightAngle.z;

        while (leftAngle > 270.0f)
        {
            leftAngle -= rotationSpeed * Time.deltaTime;
            rightAngle += rotationSpeed * Time.deltaTime;
            m_Needles[0].transform.localEulerAngles = new Vector3(m_InitLeftAngle.x, m_InitLeftAngle.y, leftAngle);
            m_Needles[1].transform.localEulerAngles = new Vector3(m_InitRightAngle.x, m_InitRightAngle.y, rightAngle);
            yield return null;
        }
        while (leftAngle < m_InitLeftAngle.z)
        {
            leftAngle += rotationSpeed * Time.deltaTime;
            rightAngle -= rotationSpeed * Time.deltaTime;
            m_Needles[0].transform.localEulerAngles = new Vector3(m_InitLeftAngle.x, m_InitLeftAngle.y, leftAngle);
            m_Needles[1].transform.localEulerAngles = new Vector3(m_InitRightAngle.x, m_InitRightAngle.y, rightAngle);
            yield return null;
        }
    }
}
