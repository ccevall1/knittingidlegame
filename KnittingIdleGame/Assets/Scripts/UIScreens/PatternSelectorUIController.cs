﻿using UnityEngine;
using System.Collections;

public class PatternSelectorUIController : MonoBehaviour {

    private bool m_bHasBeenInitialized;
    PatternSelectorController m_PatternSelectorController;
    
	// Use this for initialization
	void Awake () {
        m_PatternSelectorController = transform.FindChild("PatternList").GetComponent<PatternSelectorController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ActivatePatternSelector()
    {
        m_PatternSelectorController.InitializePatternSelector();
    }

    public void ClosePatternSelector()
    {
        // Delete children, easier to reinitialize each time than update
        GameObject[] patternSelectors = GameObject.FindGameObjectsWithTag("PatternSelectorItem");
        foreach (GameObject patternSelector in patternSelectors)
        {
            Destroy(patternSelector);
        }
        //gameObject.SetActive(false);
    }
}
