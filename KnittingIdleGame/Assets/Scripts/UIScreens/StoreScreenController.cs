﻿using UnityEngine;
using System.Collections;

public class StoreScreenController : UIScreen {

    GameController root;

	// Use this for initialization
	void Start () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnBackButtonClicked()
    {
        root.TransitionUIScreen(GameController.UIScreenEnum.Knitting);
    }

    public override void OnScreenLoaded()
    {
        base.OnScreenLoaded();

    }
}
