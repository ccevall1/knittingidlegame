﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopUpController : MonoBehaviour {

    private const string BUTTON_CONFIRM = "BUTTON_CONFIRM";
    private const string BUTTON_PURCHASE_YARN = "BUTTON_PURCHASE_YARN";

    public enum PopUpContext { None, NeedMoreProjectsInQueue, ProjectQueueIsFull, FeatureNotAvailable, NotEnoughMoney, NotEnoughYarn }

    GameController root;
    private Text m_tPopupText;
    private PopUpContext m_ePopupContext;
    private Text m_tButtonConfirmText;

    private int m_nMissingYarnAmount;
    private PlayerDataManager.YarnColor m_eMissingYarnColor;
    
	// Use this for initialization
	void Awake () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        m_tPopupText = transform.FindChild("Text_Popup").GetComponent<Text>();
        m_tButtonConfirmText = transform.FindChild("Button_Confirm").FindChild("Text").GetComponent<Text>();
        m_ePopupContext = PopUpContext.None;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetPopupText(string sText)
    {
        if (m_ePopupContext == PopUpContext.NotEnoughYarn)
        {
            m_tButtonConfirmText.text = Localizer.Loc(BUTTON_PURCHASE_YARN);
        }
        else
        {
            m_tButtonConfirmText.text = Localizer.Loc(BUTTON_CONFIRM);
        }
        m_tPopupText.text = sText;
    }

    public void SetPopUpContext(PopUpContext eContext)
    {
        m_ePopupContext = eContext;
    }

    public void SetMissingYarnData(PlayerDataManager.YarnColor eColor, int nMissingAmount)
    {
        m_eMissingYarnColor = eColor;
        m_nMissingYarnAmount = nMissingAmount;
    }

    public void OnConfirmClicked()
    {
        if (m_nMissingYarnAmount > 0)
        {
            root.TryPurchaseYarn(m_eMissingYarnColor, )
            m_eMissingYarnColor = PlayerDataManager.YarnColor.None;
            m_nMissingYarnAmount = 0;
        }
        root.ClosePopUp(m_ePopupContext);
    }

    public void OnCloseButtonClicked()
    {
        root.ClosePopUp(m_ePopupContext);
    }
}
