﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProjectsScreenController : UIScreen {

    private const string HEADER_NEXT_UP = "HEADER_NEXT_UP";

    GameController root;
    ProjectQueueManager m_ProjectQueueManager;
    PatternSelectorUIController m_PatternSelectorUIController;
    Text m_tNextUpText;

	// Use this for initialization
	void Awake () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        m_ProjectQueueManager = transform.FindChild("ProjectQueueManager").GetComponent<ProjectQueueManager>();
        m_tNextUpText = GameObject.Find("Text_Next").GetComponent<Text>();
        m_PatternSelectorUIController = GameObject.Find("PatternSelectorControl").GetComponent<PatternSelectorUIController>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void ActivatePatternSelector()
    {
        m_PatternSelectorUIController.gameObject.SetActive(true);
        m_PatternSelectorUIController.ActivatePatternSelector();
    }

    public void ClosePatternSelector()
    {
        m_PatternSelectorUIController.ClosePatternSelector();
    }

    public override void OnBackButtonClicked()
    {
        m_PatternSelectorUIController.ClosePatternSelector();
        root.TransitionUIScreen(GameController.UIScreenEnum.Knitting);
    }

    public void OnYarnStoreButtonClicked()
    {
        root.TransitionUIScreen(GameController.UIScreenEnum.Yarn);
    }

    public override void OnScreenLoaded()
    {
        base.OnScreenLoaded();
        m_ProjectQueueManager.InitializeProjectQueue();
        m_tNextUpText.text = Localizer.Loc(HEADER_NEXT_UP);
        m_PatternSelectorUIController.ClosePatternSelector();
        m_PatternSelectorUIController.ActivatePatternSelector();
    }

    public override void RefreshScreen()
    {
        base.RefreshScreen();
        m_ProjectQueueManager.InitializeProjectQueue();
    }


}
