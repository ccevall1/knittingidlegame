﻿using UnityEngine;
using System.Collections;
using System;

public class YarnScreenController : UIScreen {

    GameController root;
    YarnStoreSelectorController m_YarnSelector;

	// Use this for initialization
	void Awake () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        m_YarnSelector = GameObject.Find("Grid_Parent").GetComponent<YarnStoreSelectorController>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnBackButtonClicked()
    {
        // TODO: it would be better if it knew where you came from and returned you there instead
        root.TransitionUIScreen(GameController.UIScreenEnum.Knitting);
    }

    public override void OnScreenLoaded()
    {
        base.OnScreenLoaded();
        YarnTemplatesList yarnTemplates = root.GetYarnTemplatesList();
        m_YarnSelector.SetYarnsFromTemplate(yarnTemplates, root.GetPlayerData());
    }

    public override void RefreshScreen()
    {
        base.RefreshScreen();
        YarnTemplatesList yarnTemplates = root.GetYarnTemplatesList();
        m_YarnSelector.SetYarnsFromTemplate(yarnTemplates, root.GetPlayerData());
    }

    public void OnUpgradeTabClicked()
    {
        root.ActivatePopUpMessage(PopUpController.PopUpContext.FeatureNotAvailable, null);
    }
}
