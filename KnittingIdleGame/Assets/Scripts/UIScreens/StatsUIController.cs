﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsUIController : UIScreen {

    GameController root;
    PlayerDataManager m_PlayerData;

    // Text
    Text m_tSkillPointsText;
    Text m_tCredPointsText;
    Text m_tMoneyText;

	// Use this for initialization
	void Start () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        m_PlayerData = root.GetPlayerData();
        m_tSkillPointsText = transform.FindChild("SkillText").FindChild("SkillXPText").GetComponent<Text>();
        m_tCredPointsText = transform.FindChild("CredText").FindChild("CredXPText").GetComponent<Text>();
        m_tMoneyText = transform.FindChild("MoneyText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void UpdateStats()
    {
        //m_tSkillPointsText.text = m_PlayerData.GetSkill().ToString();
        //m_tCredPointsText.text = m_PlayerData.GetCred().ToString();
        m_tMoneyText.text = "$" + m_PlayerData.GetPlayerMoney().ToString();
    }
}
