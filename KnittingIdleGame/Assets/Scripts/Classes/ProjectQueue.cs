﻿using System;
using System.Collections.Generic;

public class ProjectQueue {

    List<Pattern> m_Queue = new List<Pattern>();

    public ProjectQueue()
    {

    }

    public void EnqueuePattern(Pattern pattern)
    {
        m_Queue.Add(pattern);
    }

    public Pattern DequeuePattern()
    {
        Pattern ret = m_Queue[0];
        m_Queue.Remove(m_Queue[0]);
        return ret;
    }

    public Pattern PeekPattern()
    {
        return m_Queue[0];
    }

    // Used for refreshing queue ordering when manually sorting items
    public void RefreshProjectQueue(List<Pattern> qProjects)
    {
        m_Queue = qProjects;
    }

    public bool IsQueueEmpty()
    {
        return m_Queue.Count == 0;
    }

    public List<Pattern> GetPatternQueue()
    {
        return m_Queue;
    }

    public int GetQueueLength()
    {
        return m_Queue.Count;
    }
}
