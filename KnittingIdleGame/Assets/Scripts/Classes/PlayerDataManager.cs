﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerDataManager {

    public enum YarnColor { None, Red, Orange, Yellow, Green, Blue, Violet };
    private int m_nPlayerSkill;
    private int m_nPlayerCred;
    private int m_nCurrentProjectRows;
    private int m_nPlayerMoney;

    private Dictionary<YarnColor, int> m_dYarnCollection;

    private int m_nMaxProjectQueueLength = 5;

    private bool m_bActiveProject;

    public PlayerDataManager()
    {
        m_dYarnCollection = new Dictionary<YarnColor, int>();
    }

    public void ClearProjectProgress()
    {
        m_nCurrentProjectRows = 0;
        m_bActiveProject = false;
    }

    public void AddSkill(int nSkillPoints)
    {
        m_nPlayerSkill += nSkillPoints;
    }

    public void AddCred(int nCredPoints)
    {
        m_nPlayerCred += nCredPoints;
    }

    public void AddProjectRows(int nRows)
    {
        m_nCurrentProjectRows += nRows;
    }

    public int GetSkill()
    {
        return m_nPlayerSkill;
    }

    public int GetCred()
    {
        return m_nPlayerCred;
    }

    public int GetCurrentProjectRows()
    {
        return m_nCurrentProjectRows;
    }

    public void AddMoney(int nMoney)
    {
        m_nPlayerMoney += nMoney;
    }

    public int GetPlayerMoney()
    {
        return m_nPlayerMoney;
    }

    public int GetMaxProjectQueueLength()
    {
        return m_nMaxProjectQueueLength;
    }

    public bool GetIsActiveProject()
    {
        return m_bActiveProject;
    }

    public void SpendMoney(int amount)
    {
        m_nPlayerMoney -= amount;
        if (m_nPlayerMoney < 0)
        {
            Debug.LogError("Error: Player has negative money!");
        }
    }

    public void StartProject()
    {
        m_bActiveProject = true;
    }

    public void AddYarn(YarnColor color, int amount)
    {
        int currYarnAmount;
        if (m_dYarnCollection.TryGetValue(color, out currYarnAmount))
        {
            amount += currYarnAmount;
            m_dYarnCollection[color] = amount;
        }
        else
        {
            m_dYarnCollection.Add(color, amount);
        }

    }

    public void RemoveYarn(YarnColor eColor, int amount)
    {
        int currYarnAmount;
        if (m_dYarnCollection.TryGetValue(eColor, out currYarnAmount))
        {
            currYarnAmount -= amount;
            if (currYarnAmount >= 0)
            {
                Debug.Log("Old yarn amount for " + eColor + " is " + m_dYarnCollection[eColor]);
                m_dYarnCollection[eColor] = currYarnAmount;
                Debug.Log("New yarn amount for " + eColor + " is " + m_dYarnCollection[eColor]);
            }
            else
            {
                Debug.Log("Error: Removing too much yarn!");
            }
        }
        else
        {
            Debug.Log("Error: Trying to remove yarn that's not there!");
        }
    }

    public bool CheckYarnAmount(YarnColor eColor, int amount, out int yarnAmount)
    {
        m_dYarnCollection.TryGetValue(eColor, out yarnAmount);
        if (yarnAmount >= amount)
        {
            return true;
        }
        return false;
    }

    public int GetYarnAmountForColor(YarnColor eColor)
    {
        int amount;
        m_dYarnCollection.TryGetValue(eColor, out amount);
        return amount;
    }

}
