﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Yarn {

    public string m_color;
    public int m_nPrice;
    private PlayerDataManager.YarnColor m_eColor = PlayerDataManager.YarnColor.None;

    public Yarn(string color, int price)
    {
        m_color = color;
        m_nPrice = price;
    }

    public PlayerDataManager.YarnColor GetColor()
    {
        if (m_eColor == PlayerDataManager.YarnColor.None)
        {
            m_eColor = HelperFunctions.GetEnumFromColorString(m_color);
        }
        return m_eColor;
    }

    public int GetPrice()
    {
        return m_nPrice;
    }
}
