﻿using System.Collections.Generic;
using UnityEngine;

public static class Localizer {

    private static Dictionary<string, string> m_TokensMap = new Dictionary<string, string>();

    private static string[] lines;

    public static void ReadLocTokensFromFile(string sFile)
    {
        lines = sFile.Split('\n');
        string[] sKeyValue;

        foreach(string line in lines)
        {
            sKeyValue = line.Split(':');
            m_TokensMap.Add(sKeyValue[0], sKeyValue[1]);
        }
    }

    public static string Loc(string sToken)
    {
        string sResult;
        bool result = m_TokensMap.TryGetValue(sToken, out sResult);
        if (!result)
        {
            sResult = "ERROR GETTING TOKEN";
        }
        return sResult;
    }
}
