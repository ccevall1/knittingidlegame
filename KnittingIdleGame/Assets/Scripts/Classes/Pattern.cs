﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Pattern {

    public string m_sPatternName;
    public int m_nPatternLength;
    public int m_nSkillPoints;
    public int m_nSellValue;
    public int m_nYarnCost;

    public Pattern(string sName, int nLength, int nSkill, int nValue, int nYarnCost)
    {
        m_sPatternName = sName;
        m_nPatternLength = nLength;
        m_nSkillPoints = nSkill;
        m_nSellValue = nValue;
        m_nYarnCost = nYarnCost;
    }

    public string GetName() { return m_sPatternName; }
    public int GetPatternLength() { return m_nPatternLength; }
    public int GetSkillPointsValue() { return m_nSkillPoints; }
    public int GetBaseSellValue() { return m_nSellValue; }
    public int GetYarnCost() { return m_nYarnCost; }

    public void Clear()
    {
        m_sPatternName = "";
        m_nPatternLength = 1;
        m_nSkillPoints = 1;
        m_nSellValue = 1;
        m_nYarnCost = 1;
    }
	
}
