﻿/**
 * For all the functions I keep wanting to use everywhere
**/
using UnityEngine;

public static class HelperFunctions {

    public static PlayerDataManager.YarnColor GetEnumFromColorString(string sColorName)
    {
        switch (sColorName)
        {
            case "Red":
                return PlayerDataManager.YarnColor.Red;
            case "Orange":
                return PlayerDataManager.YarnColor.Orange;
            case "Yellow":
                return PlayerDataManager.YarnColor.Yellow;
            case "Green":
                return PlayerDataManager.YarnColor.Green;
            case "Blue":
                return PlayerDataManager.YarnColor.Blue;
            case "Violet":
                return PlayerDataManager.YarnColor.Violet;
            default:
                return PlayerDataManager.YarnColor.None;
        }
    }

    public static string GetColorStringFromEnum(PlayerDataManager.YarnColor eColor)
    {
        switch(eColor)
        {
            case PlayerDataManager.YarnColor.Red:
                return "Red";
            case PlayerDataManager.YarnColor.Orange:
                return "Orange";
            case PlayerDataManager.YarnColor.Yellow:
                return "Yellow";
            case PlayerDataManager.YarnColor.Green:
                return "Green";
            case PlayerDataManager.YarnColor.Blue:
                return "Blue";
            case PlayerDataManager.YarnColor.Violet:
                return "Violet";
            default:
                return "";
        }
    }

    public static Color GetColorFromString(string sColor)
    {
        switch(sColor)
        {
            case "Red":
                return Color.red;
            case "Orange":
                return new Color(1.0f,.5f,0);
            case "Yellow":
                return Color.yellow;
            case "Green":
                return Color.green;
            case "Blue":
                return Color.blue;
            case "Violet":
                return new Color(1.0f, 0, 1.0f);
            default:
                return Color.white;
        }
    }

    public static Color GetColorFromEnum(PlayerDataManager.YarnColor eColor)
    {
        switch (eColor)
        {
            case PlayerDataManager.YarnColor.Red:
                return Color.red;
            case PlayerDataManager.YarnColor.Orange:
                return new Color(1.0f, 0.5f, 0);
            case PlayerDataManager.YarnColor.Yellow:
                return Color.yellow;
            case PlayerDataManager.YarnColor.Green:
                return Color.green;
            case PlayerDataManager.YarnColor.Blue:
                return Color.blue;
            case PlayerDataManager.YarnColor.Violet:
                return new Color(1.0f, 0, 1.0f);
            default:
                return Color.white;
        }
    }

}
