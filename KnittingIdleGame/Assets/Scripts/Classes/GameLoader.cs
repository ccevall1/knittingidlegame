﻿using UnityEngine;

public class GameLoader {

    private bool m_bIsLoaded;
    private PatternTemplatesList m_PatternTemplatesList;
    private YarnTemplatesList m_YarnTemplatesList;

    public GameLoader()
    {
        m_bIsLoaded = false;
    }

    public bool IsLoaded()
    {
        return m_bIsLoaded;
    }

    public void LoadGame(TextAsset[] tFiles)
    {
        //Read Patterns from JSON file
        string targetFileText = tFiles[0].text;
        m_PatternTemplatesList = JsonUtility.FromJson<PatternTemplatesList>(targetFileText);

        //Read Yarns from JSON file
        targetFileText = tFiles[1].text;
        m_YarnTemplatesList = JsonUtility.FromJson<YarnTemplatesList>(targetFileText);

        m_bIsLoaded = true;
    }

    public PatternTemplatesList GetPatternsFromGameLoad()
    {
        return m_PatternTemplatesList;
    }

    public YarnTemplatesList GetYarnsFromGameLoad()
    {
        return m_YarnTemplatesList;
    }
}
