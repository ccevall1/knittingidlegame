﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public static GameObject m_oDraggedItem;
    Vector3 m_vStartPosition;
    Transform m_tStartParent;
    CanvasGroup m_cgCanvasGroup;
    private bool m_bIsEmpty;
    Text m_tText;

    public Pattern m_oPattern;

    void Awake ()
    {
        m_cgCanvasGroup = GetComponent<CanvasGroup>();
        m_tText = GetComponent<Text>();
        m_bIsEmpty = true;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        m_oDraggedItem = gameObject;
        m_vStartPosition = transform.position;
        m_tStartParent = transform.parent;
        m_cgCanvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        m_oDraggedItem = null;
        m_cgCanvasGroup.blocksRaycasts = true;
        if (transform.parent == m_tStartParent)
        {
            transform.position = m_vStartPosition;
        }
    }

    public void SetItemText(string sText)
    {
        m_tText.text = sText;
        if (sText != "")
        {
            m_bIsEmpty = false;
        }
    }

    public bool IsEmpty()
    {
        return m_bIsEmpty;
    }

}
