﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class YarnStoreItemController : MonoBehaviour {

    Image m_iYarnSprite;
    private PlayerDataManager.YarnColor m_eColor;
    private int m_nPrice;
    private int m_nAmount;
    Text m_tPriceTag;
    Text m_tAmountTag;

    GameController root;

	// Use this for initialization
	void Awake () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        m_iYarnSprite = transform.FindChild("Yarn").GetComponent<Image>();
        m_tPriceTag = transform.FindChild("Button").FindChild("Text_Price").GetComponent<Text>();
        m_tAmountTag = transform.FindChild("Text_Amount").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetColor(PlayerDataManager.YarnColor eColor)
    {
        m_eColor = eColor;
        m_iYarnSprite.color = HelperFunctions.GetColorFromEnum(eColor);
    }

    public void SetPrice(int price)
    {
        m_nPrice = price;
        m_tPriceTag.text = "$" + price.ToString();
    }

    public void SetAmount(int amount)
    {
        m_nAmount = amount;
        m_tAmountTag.text = "x" + amount.ToString();
    }

    public void OnYarnPurchased()
    {
        root.TryPurchaseYarn(m_eColor, m_nPrice);
    }

    public int GetPrice()
    {
        return m_nPrice;
    }

    public PlayerDataManager.YarnColor GetColor()
    {
        return m_eColor;
    }

    public int GetAmount()
    {
        return m_nAmount;
    }
}
