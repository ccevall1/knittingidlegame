﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameController : MonoBehaviour {

    private const string PROJECT_QUEUE_EMPTY = "ERROR_PROJECT_QUEUE_EMPTY";
    private const string PROJECT_QUEUE_FULL = "ERROR_PROJECT_QUEUE_FULL";
    private const string FEATURE_NOT_AVAILABLE = "ERROR_FEATURE_NOT_AVAILABLE";
    private const string NOT_ENOUGH_MONEY = "ERROR_NOT_ENOUGH_MONEY";
    private const string NOT_ENOUGH_YARN = "ERROR_NEED_MORE_YARN";

    public enum UIScreenEnum {None=-1, Knitting, Store, Projects, Yarn};

    //Game Loader
    GameLoader m_GameLoader;

    //Player Data
    PlayerDataManager m_PlayerDataManager;

    // UI Screens
    public UIScreen[] aUIScreens;
    StatsUIController m_StatsUIScreen;
    PopUpController m_PopUpController;

    public TextAsset[] tJSONFiles;
    //public TextAsset tPatternTemplates;
    //public TextAsset tYarnTemplates;
    public TextAsset tLocTokens;
    
    private PatternTemplatesList m_PatternTemplates;
    private YarnTemplatesList m_YarnTemplatesList;

    private bool m_bGameIsPaused;

    private ProjectQueue m_ProjectQueue;

	// Use this for initialization
	void Start () {

    }

    void Awake()
    {
        m_StatsUIScreen = GameObject.Find("Canvas_Stats").GetComponent<StatsUIController>();
        m_PopUpController = GameObject.Find("Canvas_Popup").GetComponent<PopUpController>();
        m_PopUpController.gameObject.SetActive(false);

        m_GameLoader = new GameLoader();
        m_PlayerDataManager = new PlayerDataManager();
        m_ProjectQueue = new ProjectQueue();
        Localizer.ReadLocTokensFromFile(tLocTokens.text);

        TransitionUIScreen(UIScreenEnum.Knitting);
    }

    // Update is called once per frame
    void Update () {
	    if (!m_GameLoader.IsLoaded())
        {
            m_GameLoader.LoadGame(tJSONFiles);
            m_PatternTemplates = m_GameLoader.GetPatternsFromGameLoad();
            m_YarnTemplatesList = m_GameLoader.GetYarnsFromGameLoad();
        }
	}

    public void TransitionUIScreen(UIScreenEnum eScreen)
    {
        //First turn off all screens
        foreach (UIScreen cScreen in aUIScreens) {
            cScreen.DeactivateScreen();
        }

        aUIScreens[(int)eScreen].gameObject.SetActive(true);
        aUIScreens[(int)eScreen].OnScreenLoaded();
    }

    public void UpdateStatsScreen()
    {
        m_StatsUIScreen.UpdateStats();
    }

    public PlayerDataManager GetPlayerData()
    {
        return m_PlayerDataManager;
    }

    public ProjectQueue GetProjectQueue()
    {
        return m_ProjectQueue;
    }

    public List<Pattern> GetProjectQueueAsQueue()
    {
        return m_ProjectQueue.GetPatternQueue();
    }

    public Pattern GetNextPatternFromQueue()
    {
        return m_ProjectQueue.DequeuePattern();
    }

    public PatternTemplatesList GetPatternTemplatesList()
    {
        return m_PatternTemplates;
    }

    public YarnTemplatesList GetYarnTemplatesList()
    {
        return m_YarnTemplatesList;
    }

    public bool TryPurchaseYarn(PlayerDataManager.YarnColor eColor, int nAmount)
    {
        int cost =  nPrice;
        if (m_PlayerDataManager.GetPlayerMoney() < cost)
        {
            ActivatePopUpMessage(PopUpController.PopUpContext.NotEnoughMoney, null);
            return false;
        }

        m_PlayerDataManager.SpendMoney(cost);
        m_PlayerDataManager.AddYarn(eColor, 1); //TODO: be able to purchase varying amounts of yarn
        UpdateStatsScreen();
        RefreshScreens();
        return true;
    }

    public void RefreshScreens()
    {
        foreach(UIScreen screen in aUIScreens)
        {
            screen.RefreshScreen();
        }
    }

    public bool GameIsPaused()
    {
        return m_bGameIsPaused;
    }

    public void ActivatePopUpMessage(PopUpController.PopUpContext eContext, string[] sAdditionalData)
    {
        string sPopUpMessage = "";

        switch(eContext)
        {
            case PopUpController.PopUpContext.NeedMoreProjectsInQueue:
                sPopUpMessage = Localizer.Loc(PROJECT_QUEUE_EMPTY);
                break;
            case PopUpController.PopUpContext.ProjectQueueIsFull:
                sPopUpMessage = Localizer.Loc(PROJECT_QUEUE_FULL);
                break;
            case PopUpController.PopUpContext.FeatureNotAvailable:
                sPopUpMessage = Localizer.Loc(FEATURE_NOT_AVAILABLE);
                break;
            case PopUpController.PopUpContext.NotEnoughMoney:
                sPopUpMessage = Localizer.Loc(NOT_ENOUGH_MONEY);
                break;
            case PopUpController.PopUpContext.NotEnoughYarn:
                int nAmountMissing;
                PlayerDataManager.YarnColor eColor = HelperFunctions.GetEnumFromColorString(sAdditionalData[0]);
                sPopUpMessage = Localizer.Loc(NOT_ENOUGH_YARN);
                int.TryParse(sAdditionalData[1], out nAmountMissing);
                sPopUpMessage = sPopUpMessage.Replace("[yarn_amount]",nAmountMissing.ToString());
                if (nAmountMissing == 1)
                {
                    sPopUpMessage = sPopUpMessage.Replace("skeins", "skein");  // TODO: make less English-based
                }
                m_PopUpController.SetMissingYarnData(eColor, nAmountMissing);
                break;
            default:
                Debug.Log("Error: Invalid Context in ActivatePopUpMessage");
                break;
        }
        m_PopUpController.SetPopUpContext(eContext);
        m_PopUpController.SetPopupText(sPopUpMessage);
        m_bGameIsPaused = true;
        m_PopUpController.gameObject.SetActive(true);
    }

    public void ClosePopUp(PopUpController.PopUpContext ePopUpContext)
    {
        UIScreenEnum eToTransition = UIScreenEnum.None;
        switch(ePopUpContext)
        {
            case PopUpController.PopUpContext.NeedMoreProjectsInQueue:
                eToTransition = UIScreenEnum.Projects;
                break;
            case PopUpController.PopUpContext.NotEnoughYarn:
                //eToTransition = UIScreenEnum.Yarn;
                break;
            default:
                break;
        }
        m_bGameIsPaused = false;
        m_PopUpController.gameObject.SetActive(false);
        if (eToTransition != UIScreenEnum.None)
        {
            TransitionUIScreen(eToTransition);
        }
    }

    public void UpdateProjectQueue(List<Pattern> qProjectQueue)
    {
        m_ProjectQueue.RefreshProjectQueue(qProjectQueue);
    }
}
