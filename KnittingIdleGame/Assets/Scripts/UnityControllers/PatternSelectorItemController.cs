﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PatternSelectorItemController : MonoBehaviour {

    Text m_tPatternName;
    PatternSelectorController m_PatternSelector;
    ColorSelectorController m_ColorSelector;
    Text m_tButtonText;

    private Pattern m_PatternRef;

	// Use this for initialization
	void Awake () {
        m_tPatternName = transform.FindChild("PatternName").GetComponent<Text>();
        m_PatternSelector = GameObject.Find("PatternList").GetComponent<PatternSelectorController>();
        m_ColorSelector = transform.FindChild("ColorSelector").GetComponent<ColorSelectorController>();
        m_tButtonText = transform.FindChild("Button_SelectPattern").FindChild("Text_YarnAmount").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetPattern(Pattern pattern)
    {
        Debug.Log("Setting pattern " + pattern.GetName());
        m_PatternRef = pattern;
        m_tPatternName.text = pattern.GetName();
    }

    public void OnSelectButtonClicked()
    {
        Debug.Log("Selected " + m_PatternRef.GetName());
        Debug.Log("Yarn cost: " + m_PatternRef.GetYarnCost());
        Debug.Log("Selected color: " + m_ColorSelector.GetSelectedColor());
        //Check if player has enough yarn to start project
        m_PatternSelector.AddPatternToQueue(m_PatternRef, m_ColorSelector.GetSelectedColor());
        OnColorSelectorButtonClicked();
    }

    public void OnColorSelectorButtonClicked()
    {
        int yarnAmount;
        m_PatternSelector.CheckYarnAmount(m_ColorSelector.GetSelectedColor(),0,out yarnAmount);
        int yarnCost = m_PatternRef.GetYarnCost();
        UpdateButtonYarnAmountText(yarnAmount, yarnCost);
    }

    public void UpdateButtonYarnAmountText(int yarnAmount, int yarnCost)
    {
        if (yarnAmount < yarnCost)
        {
            m_tButtonText.color = Color.red;
        }
        else
        {
            m_tButtonText.color = Color.black;
        }
        m_tButtonText.text = yarnAmount.ToString() + "/" + yarnCost.ToString();
    }
}
