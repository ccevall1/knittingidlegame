﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class SlotController : MonoBehaviour, IDropHandler {

    public DragHandler m_oItem
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).GetComponent<DragHandler>();
            }
            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!m_oItem)
        {
            DragHandler.m_oDraggedItem.transform.SetParent(transform);
        }
    }

    public void SetItemInfo(Pattern pattern)
    {
        m_oItem.SetItemText(pattern.GetName());
    }

}
