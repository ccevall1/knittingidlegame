﻿/**
 * This class handles communication between the drag/drop interface and the GameController's internal ProjectQueue
**/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ProjectQueueManager : MonoBehaviour {

    List<Pattern> m_qProjects;
    GameController root;
    public SlotController[] m_aSlots;

    private Pattern m_EmptyPattern = new Pattern("", 0, 0, 0, 0);

	// Use this for initialization
	void Awake () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    // Send the PlayerData saved ProjectQueue to this screen to initialize, called when game loaded
    public void InitializeProjectQueue()
    {
        m_qProjects = root.GetProjectQueueAsQueue();
        for (int i = 0; i < m_aSlots.Length; i++)
        {
            if (m_qProjects.Count > i)
            {
                m_aSlots[i].SetItemInfo(m_qProjects[i]);
            }
            else
            {
                //Debug.LogWarning("Setting empty pattern: " + m_qProjects.Count + " <= " + i);
                m_aSlots[i].SetItemInfo(m_EmptyPattern);
            }
        }
    }

    // Update the GameController's ProjectQueue
    public void UpdateProjectQueue()
    {
        root.UpdateProjectQueue(m_qProjects);
        //Tell the UI to update
        InitializeProjectQueue();
    }

}
