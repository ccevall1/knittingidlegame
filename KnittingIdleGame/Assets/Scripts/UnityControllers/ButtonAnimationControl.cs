﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonAnimationControl : MonoBehaviour {

    //Button m_Button;
    float m_Timer;
	// Use this for initialization
	void Start () {
        //m_Button = GetComponent<Button>();
        m_Timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
        m_Timer += Time.deltaTime;
        float fScale = 0.1f*Mathf.Abs(Mathf.Sin(4*m_Timer)) + 1;
        transform.localScale = new Vector3(fScale, fScale, fScale);
	}
}
