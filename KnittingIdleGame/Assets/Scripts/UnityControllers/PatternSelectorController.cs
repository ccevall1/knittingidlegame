﻿using UnityEngine;
using System.Collections;

public class PatternSelectorController : MonoBehaviour {

    GameController root;
    ProjectsScreenController m_ProjectScreen;

    public GameObject m_oPatternListItemPrefab;
    public GameObject m_ContentParent;

	// Use this for initialization
	void Awake () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        m_ProjectScreen = GameObject.Find("Canvas_ProjectsScreen").GetComponent<ProjectsScreenController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void InitializePatternSelector()
    {
        PatternTemplatesList templates = root.GetPatternTemplatesList();
        foreach (Pattern pattern in templates.patterns)
        {
            PatternSelectorItemController newSelectorItem = (Instantiate(m_oPatternListItemPrefab) as GameObject).GetComponent<PatternSelectorItemController>();
            newSelectorItem.SetPattern(pattern);
            Debug.Log("Adding item " + pattern.GetName());
            newSelectorItem.transform.SetParent(m_ContentParent.transform);
            newSelectorItem.transform.localScale = Vector3.one;
            // Init yarn amounts for Red as default
            int redYarn = root.GetPlayerData().GetYarnAmountForColor(PlayerDataManager.YarnColor.Red);
            int yarnCost = pattern.GetYarnCost();
            newSelectorItem.UpdateButtonYarnAmountText(redYarn, yarnCost);
        }
    }

    public void AddPatternToQueue(Pattern pattern, PlayerDataManager.YarnColor eColor)
    {
        int yarnAmount;
        bool bEnoughYarn = CheckYarnAmount(eColor, pattern.GetYarnCost(), out yarnAmount);
        if (!bEnoughYarn)
        {
            string[] sAdditionalData = { eColor.ToString(), (pattern.GetYarnCost()-yarnAmount).ToString() };
            root.ActivatePopUpMessage(PopUpController.PopUpContext.NotEnoughYarn, sAdditionalData);
            //m_ProjectScreen.ClosePatternSelector();
            return;
        }

        ProjectQueue queue = root.GetProjectQueue();
        int maxQueueLen = root.GetPlayerData().GetMaxProjectQueueLength();
        // Check that queue is not full
        if (queue.GetQueueLength() < maxQueueLen)
        {
            queue.EnqueuePattern(pattern);
            Debug.Log("Enqueuing " + pattern.GetName());
            // Remove yarn
            int yarnCost = pattern.GetYarnCost();
            Debug.Log("Yarn cost: " + yarnCost.ToString());
            root.GetPlayerData().RemoveYarn(eColor, yarnCost);
        }
        else
        {
            root.ActivatePopUpMessage(PopUpController.PopUpContext.ProjectQueueIsFull, null);
        }
        root.RefreshScreens();
        //m_ProjectScreen.ClosePatternSelector();
    }

    public void Close()
    {
        m_ProjectScreen.ClosePatternSelector();
    }

    // Returns true if there is >= the amount of yarn needed in a color
    public bool CheckYarnAmount(PlayerDataManager.YarnColor eColor, int amount, out int yarnAmount)
    {
        return root.GetPlayerData().CheckYarnAmount(eColor, amount, out yarnAmount);
    }

}
