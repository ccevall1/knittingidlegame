﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorSelectorController : MonoBehaviour {

    PatternSelectorItemController m_PatternSelectorItem;
    PlayerDataManager.YarnColor m_eCurrColor = PlayerDataManager.YarnColor.Red;
    Text m_tColorText;
    int m_nCurrIndex = 0;

    string[] m_asColors = {"Red", "Orange", "Yellow", "Green", "Blue", "Violet" };

	// Use this for initialization
	void Awake () {
        m_PatternSelectorItem = transform.parent.GetComponent<PatternSelectorItemController>();
        m_tColorText = transform.FindChild("Text_Color").GetComponent<Text>();
	}

    public void OnLeftButtonClicked()
    {
        m_nCurrIndex = (m_nCurrIndex - 1 + m_asColors.Length) % m_asColors.Length;
        m_tColorText.text = m_asColors[m_nCurrIndex];
        m_eCurrColor = HelperFunctions.GetEnumFromColorString(m_asColors[m_nCurrIndex]);
        m_PatternSelectorItem.OnColorSelectorButtonClicked();
    }

    public void OnRightButtonClicked()
    {
        m_nCurrIndex = (m_nCurrIndex + 1) % m_asColors.Length;
        m_tColorText.text = m_asColors[m_nCurrIndex];
        m_eCurrColor = HelperFunctions.GetEnumFromColorString(m_asColors[m_nCurrIndex]);
        m_PatternSelectorItem.OnColorSelectorButtonClicked();
    }

    public PlayerDataManager.YarnColor GetSelectedColor()
    {
        return m_eCurrColor;
    }

}
