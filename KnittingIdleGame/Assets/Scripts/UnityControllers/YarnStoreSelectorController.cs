﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class YarnStoreSelectorController : MonoBehaviour {
    
    public List<YarnStoreItemController> m_aYarnItems; //Set in editor

	// Use this for initialization
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetYarnsFromTemplate(YarnTemplatesList yarnTemplates, PlayerDataManager playerData)
    {
        Yarn[] yarns = yarnTemplates.yarns;
        int i = 0;
        foreach (YarnStoreItemController yarnItem in m_aYarnItems)
        {
            yarnItem.SetColor(yarns[i].GetColor());
            yarnItem.SetPrice(yarns[i].GetPrice());
            yarnItem.SetAmount(playerData.GetYarnAmountForColor(yarns[i].GetColor()));
            i++;
        }
    }

}
