﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    AudioSource m_Source;
    TextMesh m_Text;

    public Color[] m_aColors;

    int m_nCurrColorIndex = 0;
    float m_nSoundDelay;

	// Use this for initialization
	void Start () {
        m_Source = GetComponent<AudioSource>();
        m_Text = transform.FindChild("Prompt_Text").GetComponent<TextMesh>();
        m_Text.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        bool bInRangeOfTree = false;

        RaycastHit[] rays = Physics.RaycastAll(transform.position, transform.forward, 10.0f);
        if (rays.Length > 0)
        {
            bInRangeOfTree = true;
            m_Text.gameObject.SetActive(true);
        }
        else
        {
            m_Text.gameObject.SetActive(false);
        }
        if (Input.GetButtonDown("Fire1"))
        {
            if (bInRangeOfTree)
            {
                TreeControl tree = rays[0].transform.GetComponent<TreeControl>();
                tree.ChangeColor(m_aColors[m_nCurrColorIndex]);
                RotateColor();
            }
        }
        else if (Input.GetAxis("Vertical") > 0 && m_nSoundDelay == 0)
        {
            m_Source.Play();
            m_nSoundDelay = 1.0f;
        }

        if (m_nSoundDelay > 0)
        {
            m_nSoundDelay -= Time.deltaTime;
        }
        else
        {
            m_nSoundDelay = 0;
        }
    }

    public void RotateColor()
    {
        m_nCurrColorIndex = (m_nCurrColorIndex + 1) % m_aColors.Length;
        Debug.Log("Set color to " + m_aColors[m_nCurrColorIndex]);
    }

}
