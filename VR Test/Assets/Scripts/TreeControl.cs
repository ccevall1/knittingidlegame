﻿using UnityEngine;
using System.Collections;

public class TreeControl : MonoBehaviour {

    Material m_Material;

	// Use this for initialization
	void Start () {
        m_Material = GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ChangeColor(Color color)
    {
        m_Material.color = color;
    }
}
